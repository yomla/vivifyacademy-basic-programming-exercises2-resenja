<?php

// Napisati PHP skriptu koja za date tri promenljive $a, $b i $c koje sadrže brojeve redom ispisuje brojeve od najmanjeg ka najvećem koristeći if/else.

$a = 8;
$b = 11;
$c = 20;

if ($a <= $b && $a <= $c) {
	echo $a . "<br>";

	if ($b <= $c) {
		echo $b . "<br>";
		echo $c;
	} else {
		echo $c . "<br>";
		echo $b;
	}

} elseif ($b <= $c) {
	echo $b . "<br>";

	if ($a <= $c) {
		echo $a . "<br>";
		echo $c;
	} else {
		echo $c;
		echo $a;
	}

} else {
	echo $c . "<br>";

	if ($a <= $b) {
		echo $a . "<br>";
		echo $b;
	} else {
		echo $b;
		echo $a;
	}
}


