<?php

// Napisati PHP skriptu koja kreira promenljive $a i $b i dodeljuje im vrednosti 6 i 8. U slučaju da je zbir ovih vrednosti veći od 10, ispisati zbir promenljivih, a u suprotnom ispisati njihovu razliku. Koristiti if/else

$a = 6;
$b = 8;

if ($a + $b > 10) {

	echo "Zbir je " . ($a + $b);

} else {

	echo "Razlika je " . ($a - $b);

}
