<?php

// Napraviti promenljivu $a sa vrednošću 5 i promenljivu $b sa vrednošću 9. Promeljivu $b sabrati sa stringom “4”. Ukoliko su obe promenljive tipa integer, ispisati poruku “Promenljive su tipa integer” i ispisati njihov zbir, u suprotnom ispisati “Promenljive nisu tipa integer”. Koristiti funkciju gettype i if/else

$a = 5;
$b = 9;

$b = $b + "4";

if (gettype($a) == 'integer' && gettype($b) == 'integer') {

	echo "Promenljive su tipa integer, njihov zbir iznosi: " . $b;

} else {

	echo "Promenljive nisu tipa integer";
}