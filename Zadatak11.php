<?php

// Napisati PHP skriptu koja ispisuje brojeve od 20 do 30 koristeći while petlju.

$n = 20;

while ($n <= 30) {

    echo "$n <br>";
    
    $n++;
}  