<?php

// Napisati PHP skriptu koja ispisuje vrednost kvadrata datog broja (n^2) ako je broj neparan, u suprotnom (ako je broj paran) ispisuje koren datog broja (koristiti funkciju sqrt).

$n = 5;

if ($n % 2 > 0) 
   echo "Broj je neparan i vrednost njegovog kvadrata je: " . ($n*$n);
else
   echo "Broj je paran i njegov koren je: " . (sqrt($n));