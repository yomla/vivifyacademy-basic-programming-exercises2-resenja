<?php

// Napisati PHP skriptu koja za dati integer ispisuje poruku da li je broj jednocifren, dvocifren, trocifren, paran ili neparan.

$n = 555;
$absN = abs($n);
   
if ($absN < 10) {
    
    echo "Broj je jednocifren";

} elseif ($absN < 100) {

    echo "Broj je dvocifren";

} elseif ($absN < 1000) {
    
    echo "Broj je trocifren";
}

if ($n % 2 == 0) {
        echo " i paran";
} else 
        echo " i neparan";   

    
